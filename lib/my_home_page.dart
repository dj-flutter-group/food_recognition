import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:tflite/tflite.dart';

const String ssd = "SSD MobileNet";
const String yolo = "Tiny YOLOv2";

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  File? _image;

  String _model = ssd;

  double _imageWidth = 0;
  double _imageHeight = 0;
  bool _busy = false;

  List<dynamic> _recognitions = [];

  @override
  void initState() {
    super.initState();
    _busy = true;

    loadModel().then((val) {
      setState(() {
        _busy = false;
      });
    });
  }

  loadModel() async {
    Tflite.close();
    try {
      String? res;
      if (_model == yolo) {
        res = await Tflite.loadModel(
          model: "assets/tflite/yolov2_tiny.tflite",
          labels: "assets/tflite/yolov2_tiny.txt",
        );
      } else {
        res = await Tflite.loadModel(
          model: "assets/tflite/ssd_mobilenet.tflite",
          labels: "assets/tflite/ssd_mobilenet.txt",
        );
      }
      print(res);
    } on PlatformException {
      print("Failed to load the model");
    }
  }

  Future getImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(
        source: source,
        maxHeight: 500,
        maxWidth: 500,
      );
      if (image == null) return;

      // final imageTemporary = File(image.path);
      final imagePerment = await saveFilePermanently(image.path);

      setState(() {
        _image = imagePerment;
        _busy = true;
      });
      predictImage(imagePerment);
    } on PlatformException catch (e) {
      print("Failed to pick image: $e");
    }
  }

  predictImage(File image) async {
    if (image == null) return;

    if (_model == yolo) {
      await yolov2Tiny(image);
    } else {
      await ssdMobileNet(image);
    }

    FileImage(image)
        .resolve(ImageConfiguration())
        .addListener((ImageStreamListener((ImageInfo info, bool _) {
          setState(() {
            _imageWidth = info.image.width.toDouble();
            _imageHeight = info.image.height.toDouble();
          });
        })));

    setState(() {
      _image = image;
      _busy = false;
    });
  }

  yolov2Tiny(File image) async {
    var recognitions = await Tflite.detectObjectOnImage(
        path: image.path,
        model: "YOLO",
        threshold: 0.3,
        imageMean: 0.0,
        imageStd: 255.0,
        numResultsPerClass: 1);

    setState(() {
      _recognitions = recognitions!;
    });
  }

  ssdMobileNet(File image) async {
    var recognitions = await Tflite.detectObjectOnImage(
        path: image.path, numResultsPerClass: 1);

    setState(() {
      _recognitions = recognitions!;
    });
  }

  List<Widget> renderBoxes(Size screen) {
    if (_recognitions == []) return [];
    if (_imageWidth == 0 || _imageHeight == 0) return [];

    double factorX = screen.width;
    double factorY = _imageHeight / _imageHeight * screen.width;

    Color blue = Colors.red;

    return _recognitions.map((re) {
      return Positioned(
        left: re["rect"]["x"] * factorX,
        top: re["rect"]["y"] * factorY,
        width: re["rect"]["w"] * factorX,
        height: re["rect"]["h"] * factorY,
        child: Container(
          decoration: BoxDecoration(
              border: Border.all(
            color: blue,
            width: 3,
          )),
          child: Text(
            "${re["detectedClass"]} ${(re["confidenceInClass"] * 100).toStringAsFixed(0)}%",
            style: TextStyle(
              background: Paint()..color = blue,
              color: Colors.white,
              fontSize: 15,
            ),
          ),
        ),
      );
    }).toList();
  }

// save image permanently
  Future<File> saveFilePermanently(String imagePath) async {
    final directory = await getApplicationDocumentsDirectory();
    final name = basename(imagePath);
    final image = File('${directory.path}/$name');

    return File(imagePath).copy(image.path);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    List<Widget> stackChildren = [];

    stackChildren.add(Positioned(
      top: 0.0,
      left: 0.0,
      width: size.width,
      child: Column(
        children: [
          // const SizedBox(height: 40),
          _image != null
              ? Image.file(
                  _image!,
                  // width: MediaQuery.of(context).size.width * 0.98,
                  // height: MediaQuery.of(context).size.height * 0.5,
                  // fit: BoxFit.cover,
                )
              : Column(
                  children: [
                    Icon(
                      Icons.camera,
                      color: Colors.blue,
                      size: MediaQuery.of(context).size.width * 0.5,
                    ),
                    const Text(
                      "Add an image to continue",
                      style: TextStyle(fontSize: 20),
                    ),
                  ],
                ),
          const SizedBox(height: 50),
          customButton(
            title: "Pick from gallery",
            icon: Icons.image_outlined,
            onClick: () => getImage(ImageSource.gallery),
          ),
          const SizedBox(height: 10),
          customButton(
            title: "Pick from Camera",
            icon: Icons.camera_alt_outlined,
            onClick: () => getImage(ImageSource.camera),
          ),
        ],
      ),
    ));

    // stackChildren.add(Positioned(
    //   top: 0.0,
    //   left: 0.0,
    //   width: size.width,
    //   child: _image == null
    //       ? const Text("No Image Selected")
    //       : Image.file(_image!),
    // ));

    stackChildren.addAll(renderBoxes(size));

    if (_busy) {
      stackChildren.add(const Center(
        child: CircularProgressIndicator(color: Colors.red),
      ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Colors.blue[900],
        actions: [
          PopupMenuButton(
            onSelected: (dynamic value) {
              setState(() {
                _model = value;
              });
            },
            itemBuilder: (BuildContext context) => <PopupMenuEntry>[
              const PopupMenuItem(
                child: Text("SSD MobileNet"),
                value: ssd,
              ),
              const PopupMenuItem(
                child: Text("YOLOv2 Tiny"),
                value: yolo,
              ),
            ],
          ),
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   child: Icon(Icons.image),
      //   tooltip: "Pick Image from gallery",
      //   onPressed: () => getImage(ImageSource.gallery),
      // ),
      body: Stack(
        children: stackChildren,
      ),
      bottomNavigationBar: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _busy
              ? Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  color: Colors.red,
                  child: const Text(
                    "Loading... or Failed to load model",
                    style: TextStyle(fontSize: 16),
                  ),
                )
              : const Text("Welcome to Food Recognition"),
          const SizedBox(height: 40),
        ],
      ),
    );
  }

// custom button
  Widget customButton({
    required String title,
    required IconData icon,
    required VoidCallback onClick,
  }) {
    return Container(
      width: 200,
      child: ElevatedButton(
        onPressed: onClick,
        child: Row(
          children: [
            Icon(icon),
            const SizedBox(width: 20),
            Text(title),
          ],
        ),
      ),
    );
  }
}
